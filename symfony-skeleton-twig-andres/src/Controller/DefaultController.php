<?php

namespace App\Controller;

use App\Model\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function homepage()
    {
        $tweetsRelevantes = [
            'Palma el Madrid',
            'Se disuelve el PP tras el caso de corrupción'
        ];

        $user = new User('kiko', false);

        return $this->render(
            'base.html.twig',
            [
                'mensaje' => 'Bienvenidos a Twitter',
                'relevantTweets' => $tweetsRelevantes,
                'user' => $user
            ]
        );
    }

    /**
     * @Route("/settings")
     */
    public function settings()
    {
        return $this->render('setting.html.twig');
    }

    /**
     * @Route("/users/{name}")
     */
    public function tweetsUsuario($name)
    {
        return new Response("<html><body><h1>TIMELINE!!</h1><h3>TWEETS DE $name</h3></body></html>");
    }
}
