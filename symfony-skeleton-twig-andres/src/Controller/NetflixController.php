<?php

namespace App\Controller;

use App\Model\Pelicula;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class NetflixController extends AbstractController
{
    /**
     * @Route("/netflix", name="netflix_homepage")
     */
    public function listarPeliculas()
    {
        $peliculas = $this->getPeliculas();

        return $this->render(
            'movies/list-movies.html.twig',
            [
                'movies' => $peliculas
            ]
        );
    }

    /**
     * @Route("/netflix/pelicula/{titulo}", name="movie_details")
     */
    public function detallesPelicula($titulo)
    {
        //recuperar la pelicula que tiene como titulo $titulo
        $peliculas = $this->getPeliculas();
        foreach ($peliculas as $pelicula) {
            if ($pelicula->getTitulo() == $titulo) {
                $peliculaBuscada = $pelicula;
                break;
            }
        }

        //pintar los detalles
        return $this->render(
            'movies/details-movie.html.twig',
            [
                'movie' => $peliculaBuscada
            ]
        );
    }

    private function getPeliculas(): array
    {
        return [
            new Pelicula('Joker', 'La pasión de Arthur Fleck, un hombre ignorado por la sociedad, es hacer reír a la gente. Sin embargo, una serie de trágicos sucesos harán que su visión del mundo se distorsione considerablemente convirtiéndolo en un brillante criminal.', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS06AIFiNRmBJgj9lKzCafeXlXrmnuM2hVppw&usqp=CAU'),
            new Pelicula('Amelie', 'La película narra la historia de la joven camarera Amélie Poulain, quien el mismo día que se entera de que Lady Di fallece en un accidente de tráfico, descubre que en su baño hay una pequeña caja que contiene juguetes, fotografías y cromos que un chico escondió cuarenta años atrás.', 'https://www.elseptimoarte.net/carteles/amelie_7079.jpg'),
            new Pelicula('Los Vengadores', 'Tras la aparición repentina de un enemigo que amenaza con destruir la seguridad mundial, Nick Furia, director de la agencia de paz internacional S.H.I.E.L.D., decide formar un equipo capaz de salvar un mundo al borde del caos e inicia una labor de reclutamiento a nivel global', 'https://i.blogs.es/e5ec52/los-vengadores-the-avengers-la-pelicula-foto/1366_2000.jpg'),
        ];
    }


}
