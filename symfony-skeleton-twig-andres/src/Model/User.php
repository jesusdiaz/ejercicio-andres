<?php

namespace App\Model;

class User
{
    public $nombre;
    private $admin;

    public function __construct(string $nombre, bool $admin)
    {
        $this->nombre = $nombre;
        $this->admin = $admin;
    }

    public function getAdmin()
    {
        return $this->admin;
    }
}
