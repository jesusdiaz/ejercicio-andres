<?php

namespace App\Model;

class Pelicula
{
    private $titulo;
    private $descripcion;
    private $urlImagen;

    public function __construct(string $titulo, string $descripcion, string $urlImagen)
    {
        $this->titulo = $titulo;
        $this->descripcion = $descripcion;
        $this->urlImagen = $urlImagen;
    }

    public function getTitulo(): string
    {
        return $this->titulo;
    }

    public function getDescripcion(): string
    {
        return $this->descripcion;
    }

    public function getUrlImagen(): string
    {
        return $this->urlImagen;
    }
}
