import { Component } from 'react'
import './Skills.scss'

class Skills extends Component {
  render() {
    const { languages, skills, soft_skills } = this.props.info

    const languageList = languages.map((language) => {
      return <p key={language.name}>{`${language.name}: ${language.level}`}</p>
    })
    const skillsList = skills.map((skill) => {
      return <li key={skill} className="skills__list-item">{skill}</li>
    })
    const softSkillsList = soft_skills.map((softSkill) => {
      return <li key={softSkill} className="skills__list-item"> {softSkill}</li>
    })
    return (
      <div className="skills-component">
        <div className="hard-skills__container container">
          <h2  className="title">Hard-Skills</h2>
          <ul className="skills__list">{skillsList}</ul>
        </div>
        <div className="soft-skills__container container">
          <h2 className="title">Soft-Skills</h2>
          <ul className="skills__list">{softSkillsList}</ul>
        </div>
        <div className="languages__container container">
          <h2 className="title">Languages</h2>
          {languageList}
        </div>
      </div>
    )
  }
}

export default Skills
