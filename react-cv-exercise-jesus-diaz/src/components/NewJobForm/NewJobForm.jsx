import { Component } from 'react'
import './NewJobForm.scss'

const INITIAL_STATE = {
  position: "",
  company: "",
  description: "",
}

class NewJobForm extends Component {
  state = INITIAL_STATE
  
  handleFormSubmit = (e) => {
    e.preventDefault()
    this.props.addJobExperience(this.state)
    this.setState(INITIAL_STATE)
  }
  handleInputChange = (e) => {
    const { name, value } = e.target

    this.setState(
      {
        [name]: value,
      },
      () => console.log(this.state),
    )
  }

  render() {
    return (
      <div className="job-form form-component">
        <h2 className="title">Add more experience</h2>
        
        <form onSubmit={this.handleFormSubmit}>
          <fieldset className="fieldset">
            {/* <label>Position</label> */}
            <input
              className="form-fields"
              type="text"
              id="position"
              name="position"
              onChange={this.handleInputChange}
              placeholder="Position"
            ></input>
          </fieldset>
          <fieldset className="fieldset">
            {/* <label>Company</label> */}
            <input
              className="form-fields"
              type="text"
              id="company"
              name="company"
              onChange={this.handleInputChange}
              placeholder="Company"
            ></input>
          </fieldset>
          <fieldset className="fieldset">
            {/* <label>Description</label> */}
            <textarea
              className="form-fields text-area"
              id="description"
              name="description"
              onChange={this.handleInputChange}
              placeholder="Description"
            ></textarea>
          </fieldset>
          <button type="submit" className="submit">
            Add new Job
          </button>
        </form>
      </div>
    )
  }
}

export default NewJobForm
