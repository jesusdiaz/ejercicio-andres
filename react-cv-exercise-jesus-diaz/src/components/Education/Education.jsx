import React, { Component } from 'react';
import './Education.scss'

class Education extends Component {
  
  render() {
    const {education} = this.props.info
    const educationList = education.map((section) => (
      <div key={section.title} className="education__list-item">
        <h3 className="qualification">{section.title}</h3>
        <p className="place">{section.institution}</p>
        <h5 className="placeDate">{section.years}</h5>
      </div>
    ))
    return (
      <section className="education education-component">
        <h2 className="title">Education</h2>
        <div className="education-list__container">
        {educationList}
        </div>
      </section>
    )
  }
}

export default Education
