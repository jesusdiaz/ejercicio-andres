import React, { Component } from 'react'
import './Header.scss'

class Header extends Component {
  render() {
    const { name, surname, jobPosition, imageUrl } = this.props.info
    const { phone, linkedin, email, gitlab } = this.props.info.contacts
    return (
      <header>
        <div className="header header__container">
          <div className="header__image-container">
            <img src={imageUrl} alt="myself" className="header-image" />
          </div>
          <div className="header__info">
            <div className="name">
              <h1 className="name-title">
                {name} {surname}
              </h1>
              <p className="position">{jobPosition}</p>
            </div>
            <div className="list__container">
              <ul className="header__list">
                <li className="header__list-item">{phone}</li>
                <li className="header__list-item">{email}</li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    )
  }
}

export default Header
