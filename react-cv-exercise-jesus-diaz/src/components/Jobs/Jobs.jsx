import React, { Component } from 'react'
import NewJobForm from '../NewJobForm'
import './Jobs.scss'

import Job from '../Job'

class Jobs extends Component {
  state = {
    showForm: false,
  }
  handleForm = () =>{
    this.setState({
      showForm: !this.state.showForm,
    })
    console.log('click');
    
  }
  render() {
    return (
      <section className="jobs jobs-component">
        <Job info={this.props.info} />

        <div>
        <button className="submit" onClick={this.handleForm}>
        Add more job experience</button>
        {this.state.showForm ? (
           <NewJobForm info={this.props.info} addJobExperience={this.props.addJobExperience}/>) : (null)
        }
        </div>
      </section>
    )
  }
}

export default Jobs
