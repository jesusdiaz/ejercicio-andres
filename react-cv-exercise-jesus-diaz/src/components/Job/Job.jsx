import React, { Component } from 'react'
import './Job.scss'

class Job extends Component {
  render() {
    const { experience } = this.props.info
    const experienceList = experience.map((job) => (
      <div key={job.position} className="jobs-list job-component">
        <div className="job-header">
          <h3 className="positionJob">{job.position}</h3>
          <div className="yearContainer">
            <p className="company">{job.company}</p>
            <p className="years">{job.years}</p>
          </div>
        </div>
        <div className="job-description">{job.description}</div>
      </div>
    ))
    return (
      <div>
        <h2 className="title">Experience</h2>
        {experienceList}
      </div>
    )
  }
}

export default Job
