import React, { Component } from 'react'
import './App.scss'
import Header from './components/Header'
import Education from './components/Education'
import Jobs from './components/Jobs'
import Skills from './components/Skills'

class App extends Component {
  state = {
    name: 'Jesús',
    surname: 'Díaz Alonso',
    jobPosition: 'Jr. Fullstack Developer',
    imageUrl:
      'https://pbs.twimg.com/profile_images/948960256104611840/JxGk41RU_400x400.jpg',
    contacts: 
      { phone: '650716881',
       email: 'jesus_1da@hotmail.com' ,
        },
    
    education: [
      { title: 'Bootcamp Fullstack Developer', institution: 'Upgrade-Hub', years: '2020-2021' },
      { title: 'Degree in Comunicacion Audiovisual', institution: 'University Pontificia of Salamanca', years: '2013-2019' },
      {title: 'Bachiller', institution: 'Zurbaran Institute', years: '2010-2013',},
    ],
    experience: [

      {
        position: 'Telemarketer',
        company: 'Vodafone',
        years: 'November 2019-June 2021',
        description:'I have worked in the customer service department, commercial issuance department and in the withholding department',
      },
      {
        position: 'Firefighter assistant',
        company: 'Almaraz nuclear power plant',
        years: 'June 2017-August 2017',
        description:'My position carried out the supervision of works in which a fire tool was used.',
      },
    ],
    languages: [{ name: 'English', level: 'B2' }, { name: 'Italian', level: 'B1' }],
    skills: [, 'JavaScript','React', 'Angular', 'Node'],
    soft_skills: [
      'Teamwork',
      'Problem-solving',
      'Adaptability',
      'Work Ethic',
      'Leadership',
    ],
  }

  addJobExperience = (job) => {
    this.setState({ experience: [...this.state.experience, job] })
  }
  render() {
    return (
      <div className="App">
        <Header info={this.state} />
        <main className="main">
          <div className="education-skills__container">
            <Skills info={this.state} className="skills"/>
            <Education info={this.state} className="education"/>
          </div>
          <Jobs info={this.state} addJobExperience={this.addJobExperience} />
        </main>
      </div>
    )
  }
}

export default App
